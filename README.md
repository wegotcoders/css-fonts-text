# Movember CSS Fonts & Text Practice #

Using CSS to style text content on a webpage.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open your local copy folder with Sublime, there is a HTML document, a CSS style sheet and an image file.
2. Add your style rules to style.css, this file is linked to from movember.html.
3. Add declarations to change the font family of elements in the HTML document.
4. Add declarations to change the font size of elements in the HTML document.
5. Add declarations to change the font style, transform and decoration of elements in the HTML document.
6. Try to target as many elements as possible for font/text styling.
7. You can see your progress by typing in terminal 'open movember.html'.
